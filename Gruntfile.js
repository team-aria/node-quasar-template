'use strict';

const path = require('path');

const jsFiles = [
    'Gruntfile.js',
    'server.js',
    'config/**/*.js',
    'controllers/**/*.js',
    'lib/**/*.js',
    'models/**/*.js',
    'test/**/*.js'
];

module.exports = (grunt) => {

    grunt.initConfig({
        eslint: {
            target: jsFiles,
            options: {
                fix: grunt.option('fix')
            }
        },

        mochaTest: {
            test: {
                src: ['test/**/*.js'],
                options: {
                    reporter: 'spec',
                    timeout: 5000,
                    clearRequireCache: true,
                }
            }
        },

        express: {
            dev: {
                options: {
                    script: 'server.js'
                }
            }
        },

        exec: {
            quasar: {
                cmd: 'quasar dev',
                options: {
                    cwd: path.normalize(process.cwd() + '/views')
                }
            }
        },

        watch: {
            express: {
                files: jsFiles,
                tasks: ['eslint', 'express'],
                options: {
                    spawn: false,
                },
            },
            test: {
                files: jsFiles,
                tasks: ['eslint', 'mochaTest'],
                options: {
                    spawn: false,
                },
            }
        },

        concurrent: {
            watch: {
                tasks: [['express', 'watch:express'], 'exec:quasar'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        env: {
            dev: {
                NODE_ENV: 'development'
            },
            prod: {
                NODE_ENV: 'production'
            },
            test: {
                NODE_ENV: 'test'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-env');

    grunt.registerTask('test', ['env:test', 'eslint', 'mochaTest']);
    grunt.registerTask('lint', ['env:test', 'eslint']);
    grunt.registerTask('dev-tests', ['env:dev', 'eslint', 'mochaTest', 'watch:test']);
    grunt.registerTask('default', ['env:dev', 'eslint', 'concurrent:watch']);
};
