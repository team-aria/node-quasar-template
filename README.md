# Node Quasar Template

Template for general-purpose web service setup.
* Uses ExpressJS for backend.
* Uses VueJS and Quasar for frontend.

## Features
* `eslint` for linting files.
* `grunt` for watching files during development.
* `anytv-node-mysql` as MySQL client.
* `winston` for logging.
* `mocha` and `chai` for tests.

## Usage

### Prerequisites
Install node (at least v8.11.4) and npm (at least 5.6.0).

Then install `quasar-cli` and `vue-cli` globally.

```
npm install -g @vue/cli quasar-cli
```

### Development

Install `grunt` and `forever` globally.
```
npm install -g grunt
```

Install the packages.
```
npm install
```

Run the frontend and backend server on development mode.
```
grunt
```

Running tests.
```
grunt test
```

Running ESLint.
```
grunt lint

# To apply auto-fix
grunt lint --fix
```

### Production
Install `forever` globally.
```
npm install -g forever
```

Build the frontend code.
```
cd views
quasar build
```

Running the server on production mode.
```
# On Windows CMD
set NODE_ENV=production
forever start server.js

# On Linux Bash
NODE_ENV=production forever start server.js
```
